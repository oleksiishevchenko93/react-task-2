export const getLimitAuthors = (authors, authorsList) => {
	const authorsData = authorsList.filter((author) => {
		return authors.includes(author.id);
	});
	const authorsArray = authorsData.map((author) => {
		return author.name;
	});
	let result = authorsArray.join(', ').split('');

	if (result.length > 24) {
		result.splice(24, Infinity, '...');
	}

	return result.join('');
};

export const getAuthorsData = (authors, authorsList) => {
	const authorsData = authorsList.filter((author) => {
		return authors.includes(author.id);
	});
	return authorsData.map((author) => {
		return author.name;
	});
};
