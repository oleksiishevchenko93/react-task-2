import React, { useCallback, useContext, useEffect, useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';

import { AuthContext } from '../../context/AuthContext';
import { useHttp } from '../../hooks/HttpHook';
import { getAuthorsData } from '../../helpers/authorsGetter';
import { changeDate } from '../../helpers/dateGenerator';

import styles from './CourseInfo.module.css';
import Button from '../../common/Button/Button';
import { getDuration } from '../../helpers/pipeDuration';

const CourseInfo = () => {
	const url = 'http://localhost:4000';
	const { courseId } = useParams();
	const { setMessageText, setAlert, setSeverity, token } =
		useContext(AuthContext);
	const { request, loading } = useHttp();
	const [course, setCourse] = useState({});
	const [authorsList, setAuthorsList] = useState([]);
	const navigate = useNavigate();

	const getCourse = useCallback(async () => {
		try {
			const data = await request(`${url}/courses/${courseId}`, 'GET', null, {
				Authorization: token,
			});
			setCourse(data.result);
		} catch (e) {
			setMessageText(e.message);
			setSeverity('error');
			setAlert(false);
		}
	}, [token, request]);

	const getAuthors = useCallback(async () => {
		try {
			const data = await request(`${url}/authors/all`, 'GET', null, {
				Authorization: token,
			});
			setAuthorsList(data.result);
		} catch (e) {
			setMessageText(e.message);
			setAlert(false);
		}
	}, [token, request]);

	useEffect(() => {
		getCourse();
		getAuthors();
	}, [getCourse, getAuthors]);

	if (loading) {
		return (
			<div>
				<h2>Loading...</h2>
			</div>
		);
	}

	return (
		<div className={styles.courseInfo}>
			<Button
				buttonText={'Back to courses'}
				callback={() => navigate(`/courses`)}
				className={'blue'}
			/>
			<h1 className={styles.title}>{course.title}</h1>
			<div className={styles.content}>
				<div className={styles.description}>
					<p>{course.description}</p>
				</div>
				<div className={styles.detail}>
					<p>
						<strong>ID: </strong>
						{course.id}
					</p>
					<p>
						<strong>Duration: </strong>
						{getDuration(course.duration)} hours
					</p>
					<p>
						<strong>Created: </strong>
						{changeDate(course.creationDate)}
					</p>
					<p>
						<strong>Authors: </strong>
					</p>
					{getAuthorsData(course.authors, authorsList).map((author, index) => {
						return (
							<p key={index} className={styles.author}>
								{author}
							</p>
						);
					})}
				</div>
			</div>
		</div>
	);
};

export default CourseInfo;
