import React, { useCallback, useContext, useEffect, useState } from 'react';

import Logo from './components/Logo/Logo';
import Button from '../../common/Button/Button';
import { AuthContext } from '../../context/AuthContext';
import { useHttp } from '../../hooks/HttpHook';

import styles from './Header.module.css';

const Header = () => {
	const url = 'http://localhost:4000';
	const { setMessageText, setAlert, setSeverity, token, logout, ready } =
		useContext(AuthContext);
	const { request, loading } = useHttp();
	const [user, setUser] = useState({});

	const getUser = useCallback(async () => {
		try {
			const data = await request(`${url}/users/me`, 'GET', null, {
				Authorization: token,
			});
			setUser(data.result);
		} catch (e) {
			setMessageText(e.message);
			setSeverity('error');
			setAlert(false);
		}
	}, [token, request]);

	useEffect(() => {
		getUser();
	}, [getUser]);

	if (loading || !ready) {
		return <h2>Loading...</h2>;
	}

	return (
		<div className={styles.header}>
			<Logo />
			<div className={styles.userName}>
				<h2>{user.name || 'Admin'}</h2>
				<Button buttonText='log out' className={'green'} callback={logout} />
			</div>
		</div>
	);
};

export default Header;
