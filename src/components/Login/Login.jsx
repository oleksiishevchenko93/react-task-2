import React, { useContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';

import { AuthContext } from '../../context/AuthContext';
import { useHttp } from '../../hooks/HttpHook';
import Form from '../../common/Form/Form';
import { url, BUTTONS_TEXT } from '../../constants';

const Login = () => {
	const { login } = useContext(AuthContext);
	const { request } = useHttp();
	const { setMessageText, setAlert, setSeverity } = useContext(AuthContext);
	const navigate = useNavigate();
	const [form, setForm] = useState({
		email: '',
		password: '',
	});
	const { LOGIN_LINK, LOGIN_BUTTON } = BUTTONS_TEXT;

	const loginHandler = async (event) => {
		event.preventDefault();
		try {
			const data = await request(`${url}/login`, 'POST', { ...form });
			login(data.result, data.user);

			setMessageText('Success!');
			setSeverity('success');
			setAlert(false);
			navigate('/courses');
		} catch (e) {
			setMessageText(e.message);
			setSeverity('error');
			setAlert(false);
		}
	};

	const handleChange = (event) => {
		setForm({
			...form,
			[event.target.name]: event.target.value,
		});
	};

	return (
		<div className={'login'}>
			<Form
				linkText={LOGIN_LINK}
				onChange={handleChange}
				buttonText={LOGIN_BUTTON}
				onSubmit={loginHandler}
				linkPath={'/registration'}
				form={form}
			/>
		</div>
	);
};

export default Login;
