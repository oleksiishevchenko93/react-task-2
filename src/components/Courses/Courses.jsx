import React, { useCallback, useContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

import CourseCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';
import Button from '../../common/Button/Button';
import { mockedAuthorsList, BUTTONS_TEXT } from '../../constants';
import { AuthContext } from '../../context/AuthContext';
import { useHttp } from '../../hooks/HttpHook';

import styles from './Courses.module.css';

const Courses = () => {
	const url = 'http://localhost:4000';
	const [coursesList, setCoursesList] = useState([]);
	const [searchValue, setSearchValue] = useState('');
	const [searchCoursesList, setSearchCoursesList] = useState([]);
	const [search, setSearch] = useState(false);
	const [authorsList, setAuthorsList] = useState(mockedAuthorsList);
	const { setMessageText, setAlert, setSeverity, token } =
		useContext(AuthContext);
	const { ADD_COURSE } = BUTTONS_TEXT;
	const { request } = useHttp();
	const navigate = useNavigate();

	const getCourses = useCallback(async () => {
		try {
			const data = await request(`${url}/courses/all`, 'GET', null, {
				Authorization: token,
			});
			setCoursesList(data.result);
		} catch (e) {
			setMessageText(e.message);
			setAlert(false);
		}
	}, [token, request]);

	const getAuthors = useCallback(async () => {
		try {
			const data = await request(`${url}/authors/all`, 'GET', null, {
				Authorization: token,
			});
			setAuthorsList(data.result);
		} catch (e) {
			setMessageText(e.message);
			setAlert(false);
		}
	}, [token, request]);

	useEffect(() => {
		getCourses();
		getAuthors();
	}, [getCourses, getAuthors]);

	const searchCourses = () => {
		if (!searchValue.trim()) {
			return setSearch(false);
		}
		const result = coursesList.filter((course) => {
			return (
				course.title.toLowerCase().includes(searchValue.toLowerCase()) ||
				course.id.toLowerCase().includes(searchValue.toLowerCase())
			);
		});
		setSearchCoursesList(result);
		setSearch(true);
	};

	const handleChange = (event) => {
		setSearchValue(event.target.value);
		if (!event.target.value.trim()) {
			setMessageText('Empty Value!');
			setSeverity('warning');
			setSearch(false);
		}
	};

	const showInfoCourse = (course) => {
		navigate(`/courses/${course.id}`);
	};

	const toggleCreatePage = () => {
		navigate('/courses/add');
	};

	return (
		<div className={styles.courses}>
			<div className={styles.topBar}>
				<SearchBar
					onClick={searchCourses}
					onChange={handleChange}
					value={searchValue}
				/>
				<Button
					callback={toggleCreatePage}
					buttonText={ADD_COURSE}
					className={'green'}
				/>
			</div>
			{!search &&
				coursesList.map((course) => {
					return (
						<CourseCard
							key={course.id}
							course={course}
							authorsList={authorsList}
							showInfoCourse={showInfoCourse}
						/>
					);
				})}
			{search &&
				searchCoursesList.map((course) => {
					return (
						<CourseCard
							key={course.id}
							course={course}
							authorsList={authorsList}
							showInfoCourse={showInfoCourse}
						/>
					);
				})}
		</div>
	);
};

export default Courses;
