import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { getDuration } from '../../../../helpers/pipeDuration';
import { changeDate } from '../../../../helpers/dateGenerator';
import { getLimitAuthors } from '../../../../helpers/authorsGetter';
import { BUTTONS_TEXT } from '../../../../constants';

import styles from './CourseCard.module.css';

const CourseCard = ({ course, authorsList }) => {
	const { title, description, creationDate, duration, authors } = course;
	const { SHOW_COURSE } = BUTTONS_TEXT;

	return (
		<div className={styles.courseCard}>
			<div className={styles.courseCard__content}>
				<h2>{title}</h2>
				<p>{description}</p>
			</div>
			<div className={styles.courseCard__info}>
				<p>
					<strong>Authors: </strong>
					{getLimitAuthors(authors, authorsList)}
				</p>
				<p>
					<strong>Duration: </strong>
					{getDuration(duration)} hours
				</p>
				<p>
					<strong>Created: </strong>
					{changeDate(creationDate)}
				</p>
				<Link to={`/courses/${course.id}`} className={'link'}>
					{SHOW_COURSE}
				</Link>
			</div>
		</div>
	);
};

CourseCard.propTypes = {
	course: PropTypes.object.isRequired,
	authorsList: PropTypes.array.isRequired,
};

export default CourseCard;
