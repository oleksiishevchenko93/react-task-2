import React, { useContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

import { AuthContext } from '../../context/AuthContext';
import { useHttp } from '../../hooks/HttpHook';
import Form from '../../common/Form/Form';
import { url, BUTTONS_TEXT } from '../../constants';

const Registration = () => {
	const { request } = useHttp();
	const { setMessageText, setAlert, setSeverity } = useContext(AuthContext);
	const navigate = useNavigate();
	const [form, setForm] = useState({
		email: '',
		password: '',
		name: '',
	});
	const { REGISTRATION_BUTTON, REGISTRATION_LINK } = BUTTONS_TEXT;

	const registerHandler = async (event) => {
		event.preventDefault();
		try {
			const data = await request(`${url}/register`, 'POST', { ...form });

			setMessageText(data.result || 'Success!');
			setSeverity('success');
			setAlert(false);
			navigate('/login');
		} catch (e) {
			setMessageText(e.message);
			setSeverity('error');
			setAlert(false);
		}
	};

	const handleChange = (event) => {
		setForm({
			...form,
			[event.target.name]: event.target.value,
		});
	};

	useEffect(() => {
		setAlert(true);
	}, [setAlert]);

	return (
		<div className={'login'}>
			<Form
				linkText={REGISTRATION_LINK}
				onChange={handleChange}
				buttonText={REGISTRATION_BUTTON}
				onSubmit={registerHandler}
				linkPath={'/login'}
				form={form}
				registration={true}
			/>
		</div>
	);
};

export default Registration;
