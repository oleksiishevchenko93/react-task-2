import React from 'react';
import { Route, Routes, Navigate } from 'react-router-dom';

import Courses from './components/Courses/Courses';
import Login from './components/Login/Login';
import Registration from './components/Registration/Registration';
import CreateCourse from './components/CreateCourse/CreateCourse';
import CourseInfo from './components/CourseInfo/CourseInfo';

export const useRoutes = (isAuthenticated) => {
	if (isAuthenticated) {
		return (
			<Routes>
				<Route path='/' element={<Navigate to='courses' />} />
				<Route path='courses' element={<Courses />} />
				<Route path='courses/:courseId' element={<CourseInfo />} />
				<Route path='courses/add' element={<CreateCourse />} />
				<Route path='login' element={<Login />} />
				<Route path='registration' element={<Registration />} />
				<Route path='*' element={<Navigate to='login' />} />
			</Routes>
		);
	}

	return (
		<Routes>
			<Route path='login' element={<Login />} />
			<Route path='registration' element={<Registration />} />
			<Route path='*' element={<Navigate to='login' />} />
		</Routes>
	);
};
