import React, { useEffect } from 'react';
import { BrowserRouter } from 'react-router-dom';

import { AuthContext } from './context/AuthContext';
import { useAuth } from './hooks/AuthHook';
import { useMessage } from './hooks/MessageHook';
import { Snackbars } from './common/Alert/Alert';
import { useRoutes } from './routes';
import Header from './components/Header/Header';

import './App.css';

function App() {
	const { token, user, login, logout, ready } = useAuth();
	const {
		messageText,
		setMessageText,
		alert,
		setAlert,
		severity,
		setSeverity,
	} = useMessage();
	const isAuthenticated = !!token;
	const routes = useRoutes(isAuthenticated);

	useEffect(() => {
		setAlert(true);
	}, [alert]);

	return (
		<AuthContext.Provider
			value={{
				token,
				user,
				login,
				logout,
				isAuthenticated,
				ready,
				messageText,
				setMessageText,
				alert,
				setAlert,
				severity,
				setSeverity,
			}}
		>
			<div className='container'>
				{isAuthenticated && <Header />}
				<BrowserRouter>{ready && routes}</BrowserRouter>
				{alert && (
					<Snackbars severity={severity} message={messageText} show={alert} />
				)}
			</div>
		</AuthContext.Provider>
	);
}

export default App;
