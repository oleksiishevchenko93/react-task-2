import { createContext } from 'react';

const noop = () => {};

export const AuthContext = createContext({
	token: null,
	userId: null,
	login: noop,
	logout: noop,
	isAuthenticated: false,
	messageText: 'auth context',
	setMessageText: noop,
	alert: false,
	setAlert: noop,
	severity: 'info',
	setSeverity: noop,
	ready: false,
});
