import { useState, useCallback, useEffect } from 'react';

export const useMessage = () => {
	const [messageText, setMessageText] = useState('Welcome!');
	const [alert, setAlert] = useState(false);
	const [severity, setSeverity] = useState('info');

	return {
		messageText,
		setMessageText,
		alert,
		setAlert,
		severity,
		setSeverity,
	};
};
