import { useState, useCallback, useEffect } from 'react';

const storageName = 'userData';

export const useAuth = () => {
	const [token, setToken] = useState(null);
	const [ready, setReady] = useState(false);
	const [user, setUser] = useState(null);
	// const [messageText, setMessageText] = useState('default text');

	const login = useCallback((jwtToken, userData) => {
		setToken(jwtToken);
		setUser(userData);

		localStorage.setItem(
			storageName,
			JSON.stringify({
				user: userData,
				token: jwtToken,
			})
		);
	}, []);

	const logout = useCallback(() => {
		setToken(null);
		setUser(null);

		localStorage.removeItem(storageName);
	}, []);

	useEffect(() => {
		const data = JSON.parse(localStorage.getItem(storageName));

		if (data && data.token) {
			login(data.token, data.user);
		}
		setReady(true);
	}, [login]);

	return { login, logout, token, user, ready };
};
