import * as React from 'react';
import PropTypes from 'prop-types';
import Stack from '@mui/material/Stack';
// import Button from '@mui/material/Button';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';

const Alert = React.forwardRef(function Alert(props, ref) {
	return <MuiAlert elevation={6} ref={ref} variant='filled' {...props} />;
});

export const Snackbars = ({
	show = false,
	message = 'This is a success message!',
	severity = 'success',
}) => {
	const [open, setOpen] = React.useState(show);

	// const handleClick = () => {
	// 	setOpen(true);
	// };

	const handleClose = (event, reason) => {
		if (reason === 'clickaway') {
			return;
		}

		setOpen(false);
	};

	return (
		<Stack spacing={2} sx={{ width: '100%' }}>
			{/*<Button variant="outlined" onClick={handleClick}>*/}
			{/*  Open success snackbar*/}
			{/*</Button>*/}
			<Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
				<Alert onClose={handleClose} severity={severity} sx={{ width: '100%' }}>
					{message}
				</Alert>
			</Snackbar>
		</Stack>
	);
};

Snackbars.propTypes = {
	show: PropTypes.bool.isRequired,
	message: PropTypes.string.isRequired,
	severity: PropTypes.string.isRequired,
};
