import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Input from '../Input/Input';
import Button from '../Button/Button';
import { LABEL_TEXT } from '../../constants';

import styles from './Form.module.css';

const Form = ({
	onSubmit,
	onChange,
	form,
	linkPath,
	linkText,
	buttonText,
	registration = false,
}) => {
	const { EMAIL, PASSWORD, NAME } = LABEL_TEXT;

	return (
		<form onSubmit={onSubmit} className={styles.form}>
			<div className={styles.inputField}>
				<Input
					name={EMAIL}
					id={EMAIL}
					onChange={onChange}
					labelText={EMAIL}
					value={form.email}
					placeholderText={EMAIL}
				/>
			</div>
			<div className={styles.inputField}>
				<Input
					name={PASSWORD}
					id={PASSWORD}
					onChange={onChange}
					labelText={PASSWORD}
					value={form.password}
					type={PASSWORD}
					placeholderText={PASSWORD}
				/>
			</div>
			{registration && (
				<div className={styles.inputField}>
					<Input
						name={NAME}
						id={NAME}
						onChange={onChange}
						labelText={NAME}
						value={form.name}
						placeholderText={NAME}
					/>
				</div>
			)}
			<Button buttonText={buttonText} type={'submit'} />
			<Link to={linkPath}>{linkText}</Link>
		</form>
	);
};

Form.propTypes = {
	onSubmit: PropTypes.func.isRequired,
	onChange: PropTypes.func.isRequired,
	form: PropTypes.object.isRequired,
	linkPath: PropTypes.string.isRequired,
	linkText: PropTypes.string.isRequired,
	buttonText: PropTypes.string.isRequired,
	registration: PropTypes.bool,
};

export default Form;
