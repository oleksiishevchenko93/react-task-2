import React from 'react';
import PropTypes from 'prop-types';

import styles from './Button.module.css';

const Button = ({
	buttonText,
	callback = () => {},
	className,
	type = 'button',
}) => {
	return (
		<button
			onClick={callback}
			className={`${styles.btn} ${styles[className]}`}
			type={type}
		>
			{buttonText}
		</button>
	);
};

Button.propTypes = {
	buttonText: PropTypes.string.isRequired,
	callback: PropTypes.func,
	className: PropTypes.string,
	type: PropTypes.string,
};

export default Button;
